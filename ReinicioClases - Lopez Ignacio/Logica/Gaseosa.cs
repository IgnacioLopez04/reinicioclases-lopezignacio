﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Gaseosa : Bebida
    {
        private int Azucar { get; set; }
        private bool Gas { get; set; }
        private bool TieneDescuento { get; set; }
        private int Descuento { get; set; }

        public override double ObtenerPrecioCosto()
        {
            double CostoFabricacion = 0;
            if (Azucar > 75)
            {
                CostoFabricacion = GetPrecio() * 75 / 100;
            }
            else
            {
                CostoFabricacion = GetPrecio() * 50 / 100;
            }

            return CostoFabricacion;
        }
    }
}
