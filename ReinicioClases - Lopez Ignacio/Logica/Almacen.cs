﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Almacen
    {
        List<AguaMineral> AguaMinerales = new List<AguaMineral>();
        List<Gaseosa> Gaseosas = new List<Gaseosa>();
        List<Alcohol> Alcoholes = new List<Alcohol>();

        public void CargarAgua()
        {
            AguaMinerales.Add();
        }

        public void CargarGaseosa()
        {
            Gaseosas.Add();
        }

        public void CargarAlchol()
        {
            Alcoholes.Add();
        }

        public void ObtenerBebidasPremium()
        {
            foreach(Alcohol Botella in Alcoholes)
            {
                Botella.ListaBebidasPremium();
            }
        }

        public List<ProductosOrdenados>ProductosOrdenos()
        {
            List<ProductosOrdenados> productosOrdenados = new List<ProductosOrdenados>();
            ProductosOrdenados producto = new ProductosOrdenados();

            foreach(AguaMineral agua in AguaMinerales)
            {
                producto.Nombre = agua.GetNombre();
                producto.Nombre = agua.GetMarca();
                producto.Precio = agua.ObtenerPrecioIva();

                productosOrdenados.Add(producto);
            }


            foreach (Gaseosa gaseosa in Gaseosas)
            {
                producto.Nombre = gaseosa.GetNombre();
                producto.Nombre = gaseosa.GetMarca();
                producto.Precio = gaseosa.ObtenerPrecioIva();

                productosOrdenados.Add(producto);
            }


            foreach (Alcohol alcohol in Alcoholes)
            {
                producto.Nombre = alcohol.GetNombre();
                producto.Nombre = alcohol.GetMarca();
                producto.Precio = alcohol.ObtenerPrecioIva();

                productosOrdenados.Add(producto);
            }


            return productosOrdenados.OrderByDescending(x => x.Precio).ToList();
        }
    }
}
