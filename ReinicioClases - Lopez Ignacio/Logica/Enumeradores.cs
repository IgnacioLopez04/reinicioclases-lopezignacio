﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public enum TipoBebida
    {
        AguaMineral,
        Gaseosa,
        Alcohol
    }

    public enum Nacionalidad
    {
        Nacional,
        Importada
    }
}
