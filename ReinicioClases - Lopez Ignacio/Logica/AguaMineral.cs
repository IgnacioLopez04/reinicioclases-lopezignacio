﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class AguaMineral : Bebida
    {
        private string Origen { get; set; }
        private int Pureza { get; set; }

        public override double ObtenerPrecioCosto()
        {
            double CostoFabricacion = 0;
            if (Pureza == 100)
            {
                CostoFabricacion = GetPrecio() * 33 / 100;
            }
            else
            {
                CostoFabricacion = GetPrecio() * 30 / 100;
            }

            return CostoFabricacion;
        }


    }
}
