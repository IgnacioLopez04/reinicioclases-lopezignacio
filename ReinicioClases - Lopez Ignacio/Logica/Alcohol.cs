﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Alcohol : Bebida
    {
        private int PorcentajeAlcohol { get; set; }
        private Nacionalidad nacionalidad { get; set; }

        public override double ObtenerPrecioCosto()
        {
            double CostoFabricacion = 0;
            if (nacionalidad == Nacionalidad.Nacional)
            {
                CostoFabricacion = GetPrecio() * 40 / 100;
            }
            else
            {
                CostoFabricacion = GetPrecio() * 45 / 100;
            }

            return CostoFabricacion;
        }

        public override double ObtenerPrecioIva()
        {
            return GetPrecio() + (GetPrecio()  *  21 / 100);
        }

        public List<Alcohol> ListaBebidasPremium()
        {
            List<Alcohol> BebidasPremium = new List<Alcohol>();

            if (GetLitros() >= 1 && nacionalidad  == Nacionalidad.Importada)
            {
                BebidasPremium.Add(this);
            }

            return BebidasPremium;
        }

        
    }
}
