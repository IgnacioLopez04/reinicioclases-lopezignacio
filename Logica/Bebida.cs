﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public abstract class Bebida
    {
        private int Identificador { get; set; }
        private string Nombre { get; set; }
        private decimal Litros { get; set; }
        private double Precio { get; set; }
        private string Marca { get; set; }

        public abstract double ObtenerPrecioCosto();

        public double GetPrecio()
        {
            return Precio;
        }

        public virtual double ObtenerPrecioIva()
        {
            return Precio + (Precio * 10.5);
        }

        public decimal GetLitros()
        {
            return Litros;
        }

        public string GetNombre()
        {
            return Nombre;
        }

        public string GetMarca()
        {
            return Marca;
        }
    }
}
